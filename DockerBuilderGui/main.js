const electron = require('electron')
const ipc = require('electron').ipcMain
const url = require('url')
const path = require('path')
const fs = require('fs')
const DragAndDrop = require('./js/dragAndDrop');
// const contextMenu = require('electron-context-menu');
let Range = require('./js/rangeFunctions');
//const {dialog} = electron.remote;
const HTMLpath = path.join(__dirname, '//html')
const { app, BrowserWindow, Menu, ipcMain, dialog } = electron
const dataPath = path.join(__dirname, 'data')
let savePath = path.join(__dirname, 'CreatedDockerFiles')

//set env 
process.env.NODE_ENV = 'development'
let newDir;
let mainWindow;
let viewWindow;
let rawWindow;
let addWindow;


// // context menu for the cyber range
// contextMenu({
// 	prepend: (defaultActions, params, browserWindow) => [
// 		{
// 			label: 'Add To Network',
// 			// Only show it when right-clicking images
//             visible: params.mediaType === 'image',
//             click: (event) => {
//                 console.log('running this shit');
//                 console.log(event);
//                 dragAndDrop.addNetwork(event);
//             }
// 		}
// 	]
// });

app.on('ready', function () {
    viewWindow = new BrowserWindow({
        width: 800,
        height: 600,
        title: 'The Cyber Range',
        webPreferences: {
            nodeIntegration: true //enable node commands to be used in js in the html pagess
        },
        fullscreen: true,
    });
    //load html
    viewWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'html/cyberRange.html'),
        protocol: 'file:',
        slashes: true
    }))
    viewWindow.on('Close', function () {
        viewWindow = null
    });
    viewWindow.on('focus', function () {
        let viewMenu = Menu.buildFromTemplate(rangeWindowMenu)
        //insert the new menu 
        Menu.setApplicationMenu(viewMenu)
    })
})
//handle new viewWindow creation
const createViewWindow = () => {
    viewWindow = new BrowserWindow({
        width: 800,
        height: 600,
        title: 'The Cyber Range',
        webPreferences: {
            nodeIntegration: true //enable node commands to be used in js in the html pagess
        },
        fullscreen: true,
    });
    //load html
    viewWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'html/cyberRange.html'),
        protocol: 'file:',
        slashes: true
    }))
    viewWindow.on('Close', function () {
        viewWindow = null
    });
    viewWindow.on('focus', function () {
        let viewMenu = Menu.buildFromTemplate(rangeWindowMenu)
        //insert the new menu 
        Menu.setApplicationMenu(viewMenu)
    })

}

const createBuilderView = () => {
    
    //checkFolder()
    mainWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
        },
        resizable: false,
        // Width:800,
        // Height:600
        // closable:false,
        // alwaysOnTop:true,
        // minimizable:false,
        // skipTaskbar:true,
        // simpleFullscreen: true,
        // frame:false
    })
    //Init HTML
    mainWindow.loadURL(url.format({
        pathname: path.join(HTMLpath, 'mainWindow.html'),
        protocol: 'file',
        slashes: true
    }))
    //close all windows when main is closed 
    mainWindow.on('closed', function () {
        app.quit()
    })
    mainWindow.on('focus', function () {
        const mainMenu = Menu.buildFromTemplate(mainMenuTemplate)
        //insert the new menu 
        // Menu.setApplicationMenu(mainMenu)
        Menu.setApplicationMenu(mainMenu)
    })
    //build the topbar template ie. files new window quit dev mode etc ....
    // const mainMenu = Menu.buildFromTemplate(mainMenuTemplate)
    //insert the new menu 
    // Menu.setApplicationMenu(mainMenu)
    // Menu.setApplicationMenu(mainMenu)
    GetData();
}

//handle new rawWindow creation
const createRawWindow = () => {
    rawWindow = new BrowserWindow({
        width: 800,
        height: 600,
        title: 'Raw Data Viewer',
        resizable: false,
        webPreferences: {
            nodeIntegration: true //enable node commands to be used in js in the html pages
        }
    });
    //load html
    rawWindow.loadURL(url.format({
        pathname: path.join(HTMLpath, 'rawWindow.html'),
        protocol: 'file:',
        slashes: true
    }))
    rawWindow.on('Close', function () {
        rawWindow = null
    })
    rawWindow.on('focus', function () {
        const rawMenu = Menu.buildFromTemplate(rawWindowMenu)
        //insert the new menu 
        // Menu.setApplicationMenu(rawMenu)
        Menu.setApplicationMenu(rawMenu)
    })
}
ipc.on('create:addCommand', function () { createCommandWindow() })
const createCommandWindow = () => {
    addCMDWindow = new BrowserWindow({
        width: 550,
        height: 200,
        title: 'Raw Data Viewer',
        webPreferences: {
            nodeIntegration: true //enable node commands to be used in js in the html pages
        },
        resizable: false
    });
    //load html
    addCMDWindow.loadURL(url.format({
        pathname: path.join(HTMLpath, 'addCommand.html'),
        protocol: 'file:',
        slashes: true
    }))
    addCMDWindow.on('Close', function () {
        addCMDWindow = null
    })
}
ipc.on('create:addUser', function () {
    createAddUserWindow()
})
const createAddUserWindow = () => {
    addWindow = new BrowserWindow({
        width: 450,
        height: 400,
        resizable: false,
        webPreferences: {
            nodeIntegration: true //enable node commands to be used in js in the html pages
        }
    });
    //load html
    addWindow.loadURL(url.format({
        pathname: path.join(HTMLpath, 'addUser.html'),
        protocol: 'file:',
        slashes: true
    }))
    addWindow.on('Close', function () {
        addWindow = null
    })
}
const GetData = () => {
    // let data = fs.readFileSync(TestData.json)
    //let testData = "Ubuntu:18.04"
    let data = JSON.parse(fs.readFileSync(path.join(dataPath, 'Data.json'), 'utf-8'))
    let defaultPackages = JSON.parse(fs.readFileSync(path.join(dataPath, 'DefaultPackage.json'), 'utf-8'))
    mainWindow.webContents.send('load:data', data, defaultPackages)
    console.log('sending data')
}
const createDockerFile = (DockerCommands) => {
    const REG_PIP = /(\spip\s)/
    let dockerFile = []
    let installCommands = []
    let otherCommands = []
    let lastCommand = []
    let savePath = path.join(newDir, 'DockerFile.txt')
    for (const [key, value] of Object.entries(DockerCommands)) {
        console.log(key, value)
        console.log(`type of vlaue: ${typeof (value)}`)
        if (typeof (value) == "string") {
            //console.log(`string foudn ${value}`)
            if (key == 'BuildFrom' && value.includes('from')) {
                dockerFile.unshift(value)
            }
            else if (value.includes('apt install') && REG_PIP.test(value) == false) {
                installCommands.push(value)
            }
            else if (value.substr(0, 3).includes('CMD')) {
                lastCommand.push(value)
            }
            else {
                otherCommands.push(value)
            }

        }
        else if (typeof (value) == "object") {
            //console.log('is Array')
            for (a = 0; a < value.length; a++) {
                let newCMD = value[a]

                if (key == 'BuildFrom' && newCMD.includes('from')) {
                    dockerFile.unshift(newCMD)
                }
                else if (newCMD.includes('install') && REG_PIP.test(newCMD) == false) {
                    installCommands.push(newCMD)
                }
                else if (newCMD.substr(0, 3).includes('CMD')) {
                    lastCommand.push(newCMD)
                }
                else {
                    otherCommands.push(newCMD)
                }
            }
        }
        else {
            //error
            console.log('invalid command type')
        }
    }
    dockerFile = dockerFile.concat(installCommands, otherCommands, lastCommand)
    console.log(dockerFile)
    // let file = fs.createWriteStream(path.join(newDir, 'DockerFile.txt'))
    // file.on('error', function(err) { /* error handling */ 
    //     mainWindow.webContents.send('error:', err)
    // })
    // dockerFile.forEach(function(v) { file.write(v.join(', ') + '\n'); });
    // file.end()
    fs.writeFileSync(savePath, '')
    dockerFile.forEach((cmd) => {
        let nl = `${cmd}\n`
        fs.appendFileSync(savePath, nl)
    })
    mainWindow.webContents.send('build:Successful')
}
// const endsWith = (str, suffix) => {
//     return str.match(suffix+"$")==suffix;
// }
const loadBuiltJSON = (filepath) => {
    console.log('loading data')
    let rawData = fs.readFileSync((filepath), 'utf-8')
    let data = JSON.parse(rawData)
    let name = path.basename(filepath).substr(0, path.basename(filepath).indexOf('.json'))
    // console.log(`${data}, ${name}`)
    mainWindow.webContents.send('dockerFileSelectedJSON', data, name)
}
const loadParseDockerFile = (filepath) => {
    //add parsing here 
    console.log('parsing')
    let rawData = fs.readFileSync((filepath), 'utf-8')
    data = rawData.split('\n')
    let parsedData = {}
    let newCMD
    for (a = 0; a < data.length; a++) {
        newCMD = data[a]
        console.log(newCMD)
        if (newCMD.substr(0, 5).includes('from')) {
            //parsedData.unshift(newCMD)
            parsedData['BuildFrom'] = newCMD[a]
        }
        else if (newCMD.includes('CMD')) {
            parsedData['RunCMD'] = newCMD[a]
        }
        else {
            parsedData[`RUN${a}`] = newCMD[a]
        }
    }
    mainWindow.webContents.send('dockerFileSelectedTXT', parsedData)
}

const createDockerJSON = (DockerCommands, role, dockerName, type, image) => {
    let toSave = {}
    let jsonPath = path.join(newDir, `${dockerName}.json`)
    toSave['role'] = role
    toSave['commands'] = DockerCommands
    toSave['type'] = type
    toSave['image'] = image
    fs.writeFileSync(jsonPath, JSON.stringify(toSave))
}
const transferCommand = (data) => {
    //console.log(util.inspect(data))
    mainWindow.webContents.send('addCustomCommand', data)
}
const saveDockerFile = () => {
    rawWindow.webContents.send('save:DFile')
}
const fetchDockerFileData = () => {
    let options = {
        title: "Select Directory",
        properties: ['openFile'],
        defaultPath: __dirname
    }
    dialog.showOpenDialog(rawWindow, options, fileSelectorCallback);
    // function directorySelectorCallback(filenames) {
    //     if (filenames && filenames.length > 0) {
    //        mainWindow.webContents.send('dockerFileSelected', filenames[0]);
    //     }

    // }
    function fileSelectorCallback(file) {
        //console.log(filename)
        let filename = file[0]
        console.log(filename)
        //path.extname('.json')
        if (path.basename(filename).toLowerCase() == 'dockerfile.txt') {
            //loadBuiltJSON(filename)
            let data = fs.readFileSync(filename)
            rawWindow.webContents.send('load:DockerFile', data, filename)
        }
        else {
            //loadParseDockerFile(filename)
            //error wrong file type
            rawWindow.webContents.send('error:invalidFile')
        }
    }
}
const openDockerFile = () => {
    let options = {
        title: "Select Directory",
        properties: ['openFile'],
        defaultPath: __dirname
    }
    dialog.showOpenDialog(mainWindow, options, fileSelectorCallback);
    // function directorySelectorCallback(filenames) {
    //     if (filenames && filenames.length > 0) {
    //        mainWindow.webContents.send('dockerFileSelected', filenames[0]);
    //     }

    // }
    function fileSelectorCallback(file) {
        //console.log(filename)
        filename = file[0]
        console.log(filename)
        //path.extname('.json')
        if (typeof (filename) != 'undefined') {
            if (path.extname(filename) == '.json') {
                loadBuiltJSON(filename)
            }
            else if (path.extname(filename) == '.txt') {
                loadParseDockerFile(filename)
            }
        }
    }
    //mainWindow.webContents.send('openDockerFile')
}
ipc.on('build:overWrite', function (e, DockerCommands, role, dockerName, type, image) {
    newDir = path.join(savePath, dockerName)
    //fs.mkdirSync(newDir)
    createDockerJSON(DockerCommands, role, dockerName, type, image)
    createDockerFile(DockerCommands)
})
ipc.on('fetch:data', function () {
    GetData()
})
ipc.on("transfer:addCommand", function (event, arg) {
    //console.log(`transfering ${arg.name}`)
    //console.log(data[0])
    transferCommand(arg)
})
ipc.on('illegal:nameTaken', function (event) {
    console.log('name taken')
    addWindow.webContents.send('illegal:nameTaken')
})
ipc.on('load:DockerFile', function () {
    // fs.readFileSync(path)
    fetchDockerFileData()
})
ipc.on('save:EditedDockerFile', function (e, data, savePath) {
    fs.writeFileSync(savePath, data)
})
ipc.on('create:DockerFile', function (event, DockerCommands, role, dockerName, type, image) {
    console.log(`docker command object ${DockerCommands} role: ${role} docker name ${dockerName} type ${type}`)
    newDir = path.join(savePath, dockerName)
    if (fs.existsSync(newDir)) {
        //send that the name was already taken
        mainWindow.webContents.send('illegal:fileExists', DockerCommands, role, dockerName, type, image)// write  alert requesting to overwrite
    }
    else {
        fs.mkdirSync(newDir)
        createDockerJSON(DockerCommands, role, dockerName, type, image)
        createDockerFile(DockerCommands)
    }
})
ipc.on('add:newUser', function (event, data) {
    //console.log(data)

    mainWindow.webContents.send('add:newUser', data)
})
ipc.on('open:workingDockerFile', openDockerFile)
// ipc.on('modify:subtractUserCount', function(e){
//     addWindow
// })
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            // {
            //     label: 'Load Docker File'
            // },
            {
                label: 'Quit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' :
                    'Ctrl+Q',
                click() {
                    app.quit();
                }
            },
            {
                label: 'Update',
                click() {
                    GetData();
                }
            }
        ]
    },
    {
        label: 'Open',
        submenu: [
            {
                label: 'Docker File',
                click() {
                    openDockerFile();
                }
            }
        ]
    },
    {
        label: 'View',
        submenu: [
            {
                label: 'Cyber Range',
                click() {
                    createViewWindow();
                }
            },
            {
                label: 'Raw File',
                click() {
                    createRawWindow();
                }
            }
        ]
    }
]
const rawWindowMenu = ([
    {
        label: 'File',
        submenu: [
            // {
            //     label: 'Load Docker File'
            // },
            {
                label: 'Quit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' :
                    'Ctrl+Q',
                click() {
                    app.quit();
                }
            },
            {
                label: 'Update',
                click() {
                    GetData();
                }
            },
            {
                label: 'Save',
                accelerator: process.platform === 'darwin' ? 'Cmd+S' : 'Ctrl+S',
                click() {
                    saveDockerFile();
                }
            }
        ]
    },
    {
        label: 'Open',
        submenu: [
            {
                label: 'Docker File',
                click() {
                    fetchDockerFileData();
                }
            }
        ]
    },
    {
        label: 'View',
        submenu: [
            {
                label: 'Cyber Range',
                click() {
                    createViewWindow();
                }
            },
            {
                label: 'Raw File',
                click() {
                    createRawWindow();
                }
            }
        ]
    }
]);
const rangeWindowMenu = ([
    {
        label: 'File',
        submenu: [
            // {
            //     label: 'Load Docker File'
            // },
            {
                label: 'Quit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' :
                    'Ctrl+Q',
                click() {
                    app.quit();
                }
            },
            {
                label: 'Update',
                click() {
                    GetData();
                }
            }
        ]
    },
    {
        label: 'Range Options',
        submenu: [
            {
                label: 'Save',
                accelerator: process.platform == 'darwin' ? 'Command+S' :
                    'Ctrl+S',
                async click() {
                    await Range.handleSave();
                }
            },
            {
                label: 'Save As',
                accelerator: process.platform == 'darwin' ? 'Command+Shift+S' :
                    'Ctrl+Shift+S',
                async click() {
                    await Range.handleSaveAs();
                }
            },
            {
                label: 'Open',
                accelerator: process.platform == 'darwin' ? 'Command+O' :
                    'Ctrl+O',
                async click() {
                    await Range.handleLoad();
                    await DragAndDrop.handleLoadGUI();
                }
            },
            {
                label: 'Build',
                accelerator: process.platform == 'darwin' ? 'Command+B' :
                'Ctrl+B',
            async click() {
                await Range.build();
            }
            }
        ]
    },
    {
        label: 'View',
        submenu: [
            {
                label: 'Cyber Range',
                click() {
                    createViewWindow();
                }
            },
            {
                label: 'Raw File',
                click() {
                    createRawWindow();
                }
            }
        ]
    }
]);
//if mac add empty object to menu
// if(process.platform == 'darwin'){
//     mainMenuTemplate.unshift({});//unshift adds on to begin of the array
// }
//add developer tools if not in production
if (process.env.NODE_ENV !== 'production') {
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [
            {
                label: 'toggle DevTools',
                accelerator: process.platform == 'darwin' ? 'Command+I' :
                    'Ctrl+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools()
                }
            },
            {
                role: 'reload'
            }
        ]
    })
    rangeWindowMenu.push({
        label: 'Developer Tools',
        submenu: [
            {
                label: 'toggle DevTools',
                accelerator: process.platform == 'darwin' ? 'Command+I' :
                    'Ctrl+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools()
                }
            },
            {
                role: 'reload'
            }
        ]
    })
}
const checkFolder = () => {
    if (!fs.existsSync(savePath)) {
        fs.mkdirSync(savePath)
    }
}
checkFolder()
//wconsole.log('data sent')