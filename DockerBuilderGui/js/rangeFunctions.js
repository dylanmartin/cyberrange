// Author: Dylan Martin 
// Date: 09/18/2019
// Summary: Functions that need to be exported to the main electron file
const fs = require('fs');
const electron = require('electron')
const { ipcRenderer, dialog } = electron
const os = process.platform;

// Global variable to hold location of current working file
var workingFile = "";

// Dylan Martin
// 09/22/2019
// Handles saving the current episode config in the cyber range
module.exports.handleSave = async () => {
    return new Promise(
        async (res, rej) => {
            // copy data from EpisodeConfig.json to file with time stamp
            if (workingFile === "") {
                let path = await dialog.showSaveDialog(
                    {
                        title: "Open",
                        filters: [
                            { name: 'Episode', extensions: ['json'] },
                            { name: 'All Files', extensions: ['*'] }
                        ],
                    }
                );
                workingFile = path.filePath;
            }
            console.log(workingFile);
            let file = fs.readFileSync(__dirname + '/../data/EpisodeConfig.json');
            let dataToSave = file.toString();
            fs.writeFileSync(workingFile, dataToSave);
            res();
        }
    );

}

module.exports.handleSaveAs = async () => {
    return new Promise(
        async (req, rej) => {
            let path = await dialog.showSaveDialog(
                {
                    title: "Open",
                    filters: [
                        { name: 'Episode', extensions: ['json'] },
                        { name: 'All Files', extensions: ['*'] }
                    ],
                }
            );
            workingFile = path.filePath;
            console.log(workingFile);
            let file = fs.readFileSync(__dirname + '/../data/EpisodeConfig.json');
            let dataToSave = file.toString();
            fs.writeFileSync(workingFile, dataToSave);
            res();
        }
    );
}


// Dylan Martin
// 09/22/2019
// Handles loading a saved config 
module.exports.handleLoad = async () => {
    return new Promise(
        async (res, rej) => {
            // handle opening a file on mac os
            let path = await dialog.showOpenDialog(
                {
                    properties: ['openFile'],
                    filters: [
                        { name: 'Episode', extensions: ['json'] },
                        { name: 'All Files', extensions: ['*'] }
                    ]
                }
            );
            workingFile = path.filePaths[0];
            let file = fs.readFileSync(workingFile);
            let dataToSave = file.toString();
            fs.writeFileSync(__dirname + '/../data/EpisodeConfig.json', dataToSave);
            res();
        }
    );
}

// Dylan Martin
// 09/22/2019
// Builds the episode into docker files
module.exports.build = async () => {
    return new Promise(
        async (res, rej) => {
            this.handleSave();
            let workingDir = workingFile.split('.')[0];
            // make foldler if it doeosnt already exist
            if (!fs.existsSync(workingDir)) {
                fs.mkdirSync(workingDir);
            }
            let currentBuildDir = `${workingDir}/build${Math.round(new Date().getTime() / 1000)}`
            // create a build file with the epoch time for uniqueness
            fs.mkdirSync(currentBuildDir);
            // get data from file
            let file = fs.readFileSync(__dirname + '/../data/EpisodeConfig.json');
            let dataToBuild = JSON.parse(file.toString());
            // Create RunScript
            fs.open(`${currentBuildDir}/RunEpisode.sh`, 'w', async (error, descriptor) => {
                fs.writeFileSync(descriptor, '# This script builds and runs the episode\n');
                // write in commands to run the vms
                for (let vm of dataToBuild.machines) {
                    fs.writeFileSync(descriptor, `docker build -t ${vm.id.toLowerCase().replace(/[^a-zA-Z0-9][a-zA-Z0-9_.-]/g, '')} ./${vm.id}\n`);
                }
                fs.writeFileSync(descriptor,
                    `port="3000"
                    subnet="10"
                    while [ true ]
                    do
                        cmd=\`lsof -i:$port\`
                        if [ -z "$cmd" ]
                        then
                            break
                        fi
                        port=$(( $port + 1 ))

                        server=$port
                        subnet=$(( $subnet + 1 ))
                    done\n`
                )
                ///////
                // Make sure remove any chars that are not these for docker container names
                // [a-zA-Z0-9][a-zA-Z0-9_.-]
                //////
                let id = 0;
                for (let network of dataToBuild.networks) {
                    fs.writeFileSync(descriptor, `docker network create --driver=bridge --subnet=$subnet.18.12.0/16 server$server\n`);
                    // change to if ssh
                    fs.writeFileSync(descriptor, `while [ true ]
                    do
                        cmd=\`lsof -i:$port\`
                        if [ -z "$cmd" ]
                        then
                            break
                        fi
                        port=$(( $port + 1 ))
                    done\n`)
                    let n1 = network.node1.replace(/[^a-zA-Z0-9][a-zA-Z0-9_.-]/g, '');
                    fs.writeFileSync(descriptor, `docker run --network="server$server" --ip $subnet.18.12.${id} -d -p $port:22 --name=${n1} ${n1}\n`);
                    id++;
                    fs.writeFileSync(descriptor, `while [ true ]
                    do
                        cmd=\`lsof -i:$port\`
                        if [ -z "$cmd" ]
                        then
                            break
                        fi
                        port=$(( $port + 1 ))
                    done\n`)
                    let n2 = network.node2.replace(/[^a-zA-Z0-9][a-zA-Z0-9_.-]/g, '')
                    fs.writeFileSync(descriptor, `docker run --network="server$server" --ip $subnet.18.12.${id} -d -p $port:22 --name=${n2} ${n2}\n`);
                    id++;
                    // add services that need to be started 
                }
                // Make script executable
                fs.chmodSync(`${currentBuildDir}/RunEpisode.sh`, "777");
            });
            // create unique directories and dockerfiles
            for (let vm of dataToBuild.machines) {
                let vmDir = `${currentBuildDir}/${vm.id}`;
                console.log(`the vm dir is ${vmDir}`);
                fs.mkdirSync(vmDir);
                fs.open(`${vmDir}/Dockerfile`, 'w', async (error, descriptor) => {
                    if (error) throw error;
                    // write in base image
                    // Make sure that we run the apt installs first before trying to configure anything
                    let sortedCommands = [`from ${vm.image}`]
                    let commands = [
                        'RUN apt update',
                        'RUN apt upgrade -y',
                        'RUN apt install -y curl']
                    // write in services
                    if (vm.services) {
                        for (let serv of vm.services) {
                            for (let command of serv.commands) {
                                commands.push(command)
                            }
                        }
                    }
                    // write in vulnerabilities
                    if (vm.vulnerabilities) {
                        for (let vuln of vm.vulnerabilities) {
                            for (let command of vuln.commands) {
                                commands.push(command)
                            }
                        }
                    }
                    for (let index = 0; index < commands.length; index++) {
                        let command = commands[index];
                        if (command.split(' ')[1] === 'apt') {
                            sortedCommands.push(command);
                            commands.splice(index, 1);
                            index--;
                        }
                    }

                    sortedCommands = sortedCommands.concat(commands);
                    for (let line of sortedCommands) {
                        fs.writeFileSync(descriptor, `${line}\n`);
                    }
                    fs.writeFileSync(descriptor, 'CMD tail -f /dev/null');
                });
            }
        }
    )
}
