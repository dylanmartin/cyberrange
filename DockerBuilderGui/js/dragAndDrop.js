// Author: Dylan Martin
// Summary: Code for cyber range window 
//          Imported in cyberRange.html 

const fs = require('fs');
const electron = require('electron');
const $ = require('jquery');
// Location of config file
const configFile = './data/EpisodeConfig.json';
// Load config from file
// Global variable keeps track of the configuration in the view. 
// I know this is shitty but it works..... plz dont touch
let conf = {
    machines: [],
    networks: []
};
// Keeps track of the the amount of sprites on screen
let count = 0;
// keeps track of the number of networks
let networkCount = 0;
// determines if user is drawing a netowrk
let drawmode = false;

let linedrawcords = {};

// keeps track of the current properties menu id so we can just hide ones not in use instead of regenerating them
let currentMenuId;

// Dylan Martin 
// 08/12/2019
// Allows objects to be dropped on the drag area
function allowDrop(ev) {
    ev.preventDefault();
}

// Dylan Martin
// 08/12/2019
// allows the sprites to be dragged and updates the position of the sprite
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
    ev.dataTransfer.setData("application/json", JSON.stringify([ev.target.id, (ev.offsetX || ev.clientX - $(ev.target).offset().left), (ev.offsetY || ev.clientY - $(ev.target).offset().top)]));
}

// Dylan Martin
// 10/01/2019
// Called whenever the mouse is moved. Currently is used to let users know they are drawing a network
handleMouseMove = (ev) => {
    if (drawmode) {
        let prevLine = document.getElementById(`network_${networkCount}`);
        if (prevLine) {
            let parent = document.getElementById('dragArea');
            parent.removeChild(prevLine);
        }
        $('#dragArea').line(linedrawcords.startx, linedrawcords.starty, ev.clientX, ev.clientY, { zindex: 0, id: `network_${networkCount}` });

    }
}

// Dylan Martin
// 08/12/2019
// Handles the logic whene the sprite is released
function drop(ev) {
    ev.preventDefault();
    let sprite;
    let services, vulnerabilities = [];
    var name = ev.dataTransfer.getData("text");
    var data = JSON.parse(ev.dataTransfer.getData("application/json"));
    ev.target.appendChild(document.getElementById(data[0]));
    let element = document.getElementById(name);
    // updates the objects current location
    element.style.left = ev.clientX - data[1] + 'px';
    element.style.top = ev.clientY - data[2] + 'px';
    element.style.position = 'absolute';
    element.style.webkitUserSelect = 'all';
    element.style.zindex = 10;
    element.onclick = (event) => handleClick(event);
    element.className = 'sprite';
    element.onmousedown = (event) => handleMouseEvent(event);
    let arr = element.id.split('_');
    // update id of component if dragged from bar to drag area
    if (element.getAttribute('hasBeenClicked') === 'false') {
        // rename id of object just placed
        let newId = ''
        if (arr.length > 1) {
            for (let i = 0; i < arr.length - 1; i++) {
                newId += arr[i];
            }
        } else {
            newId = name;
        }
        element.id = newId + '_' + count;
        for (let i = 0; i < element.childNodes.length; i++) {
            element.childNodes[i].name = newId + '_' + count;
            element.childNodes[i].id = newId + '_' + count;
            if (element.childNodes[i].nodeName === "IMG") {
                sprite = element.childNodes[i].src;
                break;
            }
        }
        element.setAttribute('hasBeenClicked', 'true');
        count++;
        loadSprites();
    }
    // update the position of the lines
    else if (element.getAttribute('hasBeenClicked') === 'true') {
        let networkId;
        for (let i = 0; i < element.childNodes.length; i++) {
            if (element.childNodes[i].nodeName === "IMG") {
                sprite = element.childNodes[i].src;
                break;
            }
        }
        // iterate through every network and machine to perserve any data that needs to remain the same
        let rawData = fs.readFileSync(configFile, 'utf8');
        let data = JSON.parse(rawData);
        for (let machine of data.machines) {
            if (machine.id === element.id) {
                // pass services and vulnerabilities into new right
                if (machine.services) {
                    services = machine.services;
                }
                if (machine.vulnerabilities) {
                    vulnerabilities = machine.vulnerabilities;
                }
            }
        }
        // iterate through ever fucking object in the episode config.... i know this is dumb will fix later
        // TODO: Fix this shit
        for (let obj of data.networks) {
            networkId = obj.id;
            if (obj.node1 === element.id || obj.node2 === element.id) {
                console.log(networkId);
                document.getElementById(networkId).remove();
                if (obj.node1 === element.id) {
                    // update node 1
                    $('#dragArea').line(ev.clientX, ev.clientY, obj.x2, obj.y2, { zindex: 0, id: networkId });
                    // update config file
                    obj.x1 = ev.clientX;
                    obj.y1 = ev.clientY;
                }
                else if (obj.node2 === element.id) {
                    // update node 2
                    $('#dragArea').line(obj.x1, obj.y1, ev.clientX, ev.clientY, { zindex: 0, id: networkId });
                    // update config file
                    obj.x2 = ev.clientX;
                    obj.y2 = ev.clientY;
                }
                addData(obj);
                // label the network with the same name
                let line = document.getElementById(networkId);
                line.onclick = (e) => handleClick(e);
                line.innerHTML = networkId;
                line.setAttribute('hasBeenClicked', false);
            }
        }
    }
    addData({
        xpos: element.style.left,
        ypos: element.style.top,
        image: element.getAttribute('image'),
        id: element.id,
        name: element.name,
        sprite: sprite,
        network: 'none',
        services: services,
        vulnerabilities: vulnerabilities
    });
}
// Dylan Martin 
// 08/14/2019
// Handles the click event
function handleClick(event) {
    let target = document.getElementById(event.target.id);
    if (drawmode === true) {
        let prevLine = document.getElementById(`network_${networkCount}`);
        if (prevLine) {
            let parent = document.getElementById('dragArea');
            parent.removeChild(prevLine);
        }
        let id = `network_${networkCount}`;
        $('#dragArea').line(linedrawcords.startx, linedrawcords.starty, event.clientX, event.clientY, { zindex: 0, id: id });
        let line = document.getElementById(id);
        line.onclick = (e) => handleClick(e);
        line.innerHTML = id;
        line.setAttribute('hasBeenClicked', false);
        drawmode = false;
        networkCount++;
        let network = {
            id: id,
            node1: linedrawcords.id,
            x1: linedrawcords.startx,
            y1: linedrawcords.starty,
            node2: event.target.id,
            x2: event.clientX,
            y2: event.clientY
        }
        addData(network);
        linedrawcords = {};
    }

    let oldSel = document.getElementsByClassName('active')
    for (let i = 0; i < oldSel.length; i++) {
        oldSel[i].classList.remove('active');
    }
    target.classList.add('active');
}

// Dylan Martin
// 10/01/2019
// Handles the create network click on the docker builder gui right click menu
addNetwork = (id, x, y) => {
    //toggle drawing mode
    if (drawmode !== true) {
        drawmode = true;
        linedrawcords.startx = x;
        linedrawcords.starty = y;
        linedrawcords.id = id;
    }
    removePropertiesMenu();
    removeDropDownMenu();
}

// Dylan Martin
// 09/15/2019
// Handles a mouse click event for the sprites on the drop area in the cyber range
function handleMouseEvent(event) {
    removeDropDownMenu();

    // add device to network on right click
    if (event.which === 3) {
        createDropdownMenu(event);
        let target;
        let oldSel = document.getElementsByClassName('active')
        if (event.target.id.endsWith('_Img')) {
            target = document.getElementById(event.target.id.replace('_Img', ''));
        }
        else if (event.target.id.endsWith('_Label')) {
            target = document.getElementById(event.target.id.replace('_Label', ''));
        }
        else {
            target = document.getElementById(event.target.id);
        }
        for (let i = 0; i < oldSel.length; i++) {
            oldSel[i].classList.remove('active');
        }
        target.classList.add('active');
    }
}

// Dylan Martin
// 10/1/2019
// Creates a drop down menue to be displayed when right clicking a sprite
createDropdownMenu = (event) => {
    // define parts fo the menu
    let menu = document.createElement('div');
    let propertiesCell = document.createElement('div');
    let deleteCell = document.createElement('div');
    let networkCell = document.createElement('div');
    menu.className = 'dropDownMenu';
    menu.id = 'dropDownMenu';
    propertiesCell.className = 'dropDownMenuCell';
    deleteCell.className = 'dropDownMenuCell';
    networkCell.className = 'dropDownMenuCell';
    menu.style.top = `${event.clientY}px`;
    menu.style.left = `${event.clientX}px`;
    //TODO: add some math here to make sure that the menu doesnt go off screen
    propertiesCell.innerHTML = "Properties";
    propertiesCell.onclick = () => {
        createPropertiesMenu(event.target);
        // document.getElementById('fileInput').addEventListener('change', handleFileSelect, false);
    };
    deleteCell.innerHTML = "Delete";
    deleteCell.onclick = (event) => { deleteContainer(event) };
    networkCell.innerHTML = "Add Network";
    networkCell.onclick = () => addNetwork(event.target.id, event.clientX, event.clientY);
    menu.appendChild(networkCell);
    menu.appendChild(propertiesCell);
    menu.appendChild(deleteCell);
    document.getElementById('dragArea').appendChild(menu);
}

// Dylan Martin
// 10/07/2019
// Creates the vm properties dropdown menu
createPropertiesMenu = (target) => {
    removePropertiesMenu();
    let m;
    if (target.id.endsWith('_Img')) {
        m = document.getElementById(`${target.id.replace('_Img', '')}propertiesMenu`);
    }
    else if (target.id.endsWith('_Label')) {
        m = document.getElementById(`${target.id.replace('_Label', '')}propertiesMenu`);
    }
    else {
        m = document.getElementById(`${target.id}propertiesMenu`);
    }
    // if menu has already been created just show it
    if (m) {
        console.log(`menu already exists with id of ${m.id}`);
        console.log(currentMenuId);
        if (document.getElementById(currentMenuId)) {
            document.getElementById(currentMenuId).style.display = 'none';
        } currentMenuId = `${m.id}`;
        m.style.display = 'block';
    } else {
        let vulnerabilities = getVulnerabilities(target.id.split("_")[0]);
        let services = getServices(target.id.split("_")[0]);
        let menu = document.createElement('div');
        menu.className = 'menu col s6 offset-s6';
        menu.id = `${target.id}propertiesMenu`;
        currentMenuId = menu.id;
        let container = document.createElement('div');
        container.className = 'propertiesContainer';
        let name = target.id;
        container.innerHTML = `<h5 class="menuTitle">Name:</h5> <span class="menuTitle" >${name}</span>`;
        menu.appendChild(container);
        // create drop dropdown to add vulnerabilities
        let vlunContainer = document.createElement('div');
        vlunContainer.id = 'vulnContainer';
        vlunContainer.innerHTML = "<h5 class='menuTitle'>Vulnerabilities:<h5>";
        for (let opt of vulnerabilities) {
            let option = document.createElement('div');
            option.id = opt.name;
            option.className = 'optionSel';
            let box = document.createElement('input');
            box.className ='optionCheckbox';
            box.type = 'checkbox';
            // check to see if the vulnerabilities are already added and if so make sure the option is checked

            option.onclick = (event) => {
                if (box.checked) {
                    box.checked = '';
                    option.className = 'optionSel';
                }
                else {
                    box.checked = 'checked';
                    option.className = 'optionSel checked';
                }
            }
            let span = document.createElement('span');
            span.className = 'optionSpan';
            span.innerHTML = opt.name;
            option.appendChild(box);
            option.appendChild(span);
            vlunContainer.appendChild(option);
        }
        menu.appendChild(vlunContainer);
        // get the services that the user can run on that vm
        let servContainer = document.createElement('div');
        servContainer.innerHTML = "<h5 class='menuTitle'>Services:<h5>";
        for (let opt of services) {
            let option = document.createElement('div');
            option.className = 'optionSel';
            option.id = opt.name;
            let box = document.createElement('input');
            box.className ='optionCheckbox';
            box.type = 'checkbox';
            option.onclick = (event) => {
                if (box.checked) {
                    box.checked = "";
                    option.className = 'optionSel';
                }
                else {
                    box.checked = 'checked';
                    option.className = 'optionSel checked';
                }
            }
            let span = document.createElement('span');
            span.className = 'optionSpan';
            span.innerHTML = opt.name;
            option.appendChild(box);
            option.appendChild(span);
            servContainer.appendChild(option);
        }
        menu.appendChild(servContainer);
        // logic for adding files to be uploaded onto the docker container
        let fileContainer = document.createElement('div');
        let addFileButton = document.createElement('a');
        let icon = document.createElement('i');
        icon.className = "material-icons";
        icon.innerHTML = "+";
        addFileButton.className = "btn-floating btn-small waves-effect waves-light"
        addFileButton.style.backgroundColor = "#8787ff";
        // addFileButton.innerHTML = "+"
        addFileButton.onclick = (event) => {
            console.log('running')
            document.getElementById('fileInput').click();
        }
        addFileButton.appendChild(icon);
        let fileInput = document.createElement('input');
        fileInput.type = 'file';
        fileInput.id = 'fileInput';
        fileInput.onchange = (event) => {
            uploadFile(fileInput.files);
        }
        fileContainer.appendChild(addFileButton);
        fileContainer.appendChild(fileInput);
        menu.appendChild(fileContainer);
        // logic for the button 
        let button = document.createElement('button');
        button.innerHTML = 'Ok';
        button.onclick = (event) => {
            for (let machine of conf.machines) {
                if (machine.id === name) {
                    for (let option of vlunContainer.children) {
                        if (option.className.includes('optionSel')) {
                            if (option.className.includes('checked')) {
                                let commands = [];
                                for (let opt of vulnerabilities) {
                                    if (opt.name === option.id) {
                                        commands = opt.commands;
                                    }
                                }
                                if (machine.vulnerabilities) {
                                    machine.vulnerabilities.push({
                                        name: option.id,
                                        commands: commands
                                    });
                                }
                                else {
                                    machine.vulnerabilities = [
                                        {
                                            name: option.id,
                                            commands: commands
                                        }
                                    ]
                                }

                            }
                        }
                    }
                    for (let option of servContainer.children) {
                        if (option.className.includes('optionSel')) {
                            if (option.className.includes('checked')) {
                                let commands = [];
                                for (let opt of services) {
                                    if (opt.name === option.id) {
                                        commands = opt.commands;
                                    }
                                }
                                if (machine.services) {
                                    machine.services.push({
                                        name: option.id,
                                        commands: commands
                                    });
                                }
                                else {
                                    machine.services = [
                                        {
                                            name: option.id,
                                            commands: commands
                                        }
                                    ]
                                }

                            }
                        }
                    }
                }
                addData(machine);
            }
            removePropertiesMenu();
        }
        menu.appendChild(button);
        document.getElementById('dragArea').appendChild(menu);
    }
}

// Dylan Martin
// 10/07/2019
// Removes the properties menu from the cyber range document
removePropertiesMenu = () => {
    let menu = document.getElementById(currentMenuId);
    console.log(currentMenuId)
    if (menu) {
        menu.style.display = 'none';
        // let parent = document.getElementById('dragArea');
        // parent.removeChild(menu);
    }
}

// Dylan Martin
// 10/01/2019
// Removes the drop down menu from the cyber range document
removeDropDownMenu = () => {
    let menu = document.getElementById('dropDownMenu');
    if (menu) {
        let parent = document.getElementById('dragArea');
        parent.removeChild(menu);
    }
}

// Dylan Martin
// 11/17/2019
// Shows/Hides the sprites in the item list
showItems = (event) => {
    let list = document.getElementById(event.target.id.split("_")[0]);
    console.log(list);
    if (list.style.display !== 'flex') {
        list.style.display = 'flex';
    }
    else {
        list.style.display = 'none';
    }
}

// Dylan Martin
// 12/30/2019
// Uploads files top be run by the container on creation
uploadFile = (fileList) => {
    for (let file of fileList) {
        var reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = function (evt) {
            console.log(evt.target.result);
        }
        reader.onerror = function (evt) {
            console.log("error reading file");
        }
    }
}

// Dylan Martin 
// 08/12/2019
// Handles initialization of the Cyber Range view
loadSprites = async () => {
    // Populates sprites from Data.json
    let obj = fs.readFileSync('./data/Data.json', 'utf8');
    let data = JSON.parse(obj);
    let spriteContainer = document.getElementById('itemList');
    spriteContainer.innerHTML = '';
    // creates an element for each container in the json object
    for (let i = 0; i < data.length; i++) {
        let container = document.createElement('div');
        let img = document.createElement('img');
        let label = document.createElement('span');
        let width = window.screen.width * .1;
        label.id = data[i].name + '_Label'
        label.draggable = false;
        label.style.maxWidth = width / 2 + 'px';
        label.style.width = width / 2 + 'px';
        label.innerHTML = data[i].name;
        label.name = data[i].name;
        label.className = 'spriteLabel'
        img.className = 'sprite';
        img.id = data[i].name + '_Img';
        img.name = data[i].name;
        img.src = data[i].sprite;
        img.style.height = width * .45 + 'px';
        img.style.width = width * .45 + 'px';
        img.draggable = false;
        container.style.margin = width * .025 + 'px';
        container.id = data[i].name;
        container.className = 'spriteContainer dropdown-item'
        container.style.maxWidth = width * .45 + 'px';
        container.setAttribute('hasBeenClicked', false);
        container.setAttribute('image', data[i].image);
        container.draggable = true;
        container.style.zIndex = 2;
        container.ondragstart = () => drag(event);
        container.appendChild(img);
        let br = document.createElement('br');
        container.appendChild(br);
        container.appendChild(label);
        spriteContainer.appendChild(container);
    }
}


// Dylan Martin
// 08/12/2019
// Handles all the keypress actions for the drag and drop gui
deleteContainer = (event) => {
    let currElems = document.getElementsByClassName('active')
    for (let i = 0; i < currElems.length; i++) {
        deleteData(currElems[i].id);
        currElems[i].remove();
    }
}

// Dylan Martin
// 10/10/2019
// Returns an array of all the possible vulnerabilities to add to the vm
// Takes in a string of the name of the container that it wants to retrieve events for.
getVulnerabilities = (container) => {
    let response = fs.readFileSync(__dirname + '/../data/Data.json');
    let data = JSON.parse(response.toString());
    for (let vm of data) {
        if (vm.name === container) {
            return vm.vulnerabilities;
        }
    }
    return [];
}

// Dylan Martin
// 10/10/2019
// Returns an array of all the possible services to add to the vm
// Takes in a string of the name of the container that it wants to retrieve events for.
getServices = (container) => {
    let response = fs.readFileSync(__dirname + '/../data/Data.json');
    let data = JSON.parse(response.toString());
    for (let vm of data) {
        if (vm.name === container) {
            return vm.services;
        }
    }
    return [];
}

// Dylan Martin
// 10/01/2019
// Listens for key inputs from the user
keyboardListener = (event) => {
    // if key is the delete key delete the selected element
    if (event.keyCode === 8) {
        let currElems = document.getElementsByClassName('active')
        for (let i = 0; i < currElems.length; i++) {
            deleteData(currElems[i].id);
            currElems[i].remove();
        }
    }
    if (event.keyCode === 27 && drawmode) {
        let prevLine = document.getElementById(`network_${networkCount}`);
        if (prevLine) {
            let parent = document.getElementById('dragArea');
            parent.removeChild(prevLine);
        }
        drawmode = false;
    }
}

// Dylan Martin
// 08/14/2019
// Removes object specified as data from the config file 
// Takes in a id of an html element to be deleted
deleteData = async (data) => {
    if (data.split('_')[0] === 'network') {
        for (let i = 0; i < conf.networks.length; i++) {
            if (data === conf.networks[i].id) {
                conf.networks.splice(i, 1);
            }
        }
    }
    else {
        for (let i = 0; i < conf.machines.length; i++) {
            if (data === conf.machines[i].id) {
                conf.machines.splice(i, 1);
            }
        }
    }
    await updateConfig(conf)
}

// Dylan Martin
// 08/14/2019
// Clears the config file storing all the episode data
clearData = async () => {
    await fs.writeFile(configFile, '{}', (err) => {
        if (err) throw err;
    });
}

// Dylan Martin
// 08/14/2019
// Adds object specified as data from the config file 
// Takes in a JOSN object
addData = async (data) => {
    console.log(data);
    // add data to EpisodeConfig.json
    let added = false;
    if (data.id.split('_')[0] === 'network') {
        for (let i = 0; i < conf.networks.length; i++) {
            if (conf.networks[i].id === data.id) {
                for (let key of Object.keys(conf.networks[i])) {
                    conf.networks[i][key] = data[key]
                    added = true;
                }
            }
        }
        if (!added) {
            conf.networks.push(data)
        }
        for (machine of conf.machines) {
            if (data.node1 === machine.id || data.node2 === machine.id) {
                machine.network = data.id;
            }
        }
    } else {
        for (let i = 0; i < conf.machines.length; i++) {
            if (conf.machines[i].id === data.id) {
                for (let key of Object.keys(conf.machines[i])) {
                    conf.machines[i][key] = data[key]
                    added = true;
                }
            }
        }
        if (!added) {
            conf.machines.push(data)
        }
    }
    // update the config file
    updateConfig(conf);
}

// Dylan Martin 
// 08/14/2019
// Writes data to config file
updateConfig = async (data) => {
    console.log(JSON.stringify(data, null, 2));
    return new Promise(
        async (res, error) => {
            await fs.writeFile(configFile, JSON.stringify(data, null, 2), (err) => {
                if (err) throw err;
            });
            res();
        }
    )
}

// Dylan Martin
// 09/25/2019
// runs handle load on document
module.exports.handleLoadGUI = async () => {
    return new Promise(
        async (res, rej) => {
            let window = electron.BrowserWindow.getFocusedWindow();
            window.webContents.executeJavaScript(`handleLoad()`);
            res();
        }
    )
}


// Dylan Martin
// 09/23/2019
// Handles rendering data loaded in from an external file
handleLoad = async () => {
    return new Promise(
        async (res, rej) => {
            let response = fs.readFileSync(configFile);
            let data = JSON.parse(response.toString());
            conf = data;
            console.log(`conf is ${JSON.stringify(conf, null, 2)}`);
            let machines = data.machines;
            let dragContainer = document.getElementById("dragArea");
            // Render in Sprites
            for (let machine of machines) {
                let container = document.createElement('div');
                let img = document.createElement('img');
                let label = document.createElement('span');
                let width = window.screen.width * .1;
                label.id = machine.id + '_Label'
                label.draggable = false;
                label.style.width = width / 2 + 'px';
                label.innerHTML = machine.id.split('_')[0];
                label.name = machine.id.split('_')[0];
                label.className = 'spriteLabel'
                img.className = 'sprite';
                img.id = machine.id + '_Img';
                img.name = machine.id.split("_")[0];
                img.src = machine.sprite;
                img.style.height = width * .45 + 'px';
                img.style.width = width * .45 + 'px';
                img.draggable = false;
                container.onmousedown = (event) => handleMouseEvent(event);
                container.style.margin = width * .025 + 'px';
                container.id = machine.id;
                container.className = 'spriteContainer'
                container.style.maxWidth = width * .45 + 'px';
                container.setAttribute('hasBeenClicked', true);
                container.draggable = true;
                container.style.zIndex = 2;
                container.ondragstart = () => drag(event);
                container.appendChild(img);
                let br = document.createElement('br');
                container.appendChild(br);
                container.appendChild(label);
                container.style.top = machine.ypos;
                container.style.left = machine.xpos;
                container.style.position = 'absolute';
                dragContainer.appendChild(container);
                loadPropertiesMenus(machine);
            }
            // Handle rendering in the network lines
            let networks = data.networks;
            networkCount = 0;
            for (let network of networks) {
                $('#dragArea').line(network.x1, network.y1, network.x2, network.y2, { zindex: 0, id: network.id });
                let line = document.getElementById(network.id);
                line.onclick = (e) => handleClick(e);
                line.innerHTML = network.id;
                line.setAttribute('hasBeenClicked', true);
                networkCount++;
            }
            res();
        }
    )
}

loadPropertiesMenus = (vm) => {
    let vulnerabilities = getVulnerabilities(vm.id.split("_")[0]);
    let services = getServices(vm.id.split("_")[0]);
    let menu = document.createElement('div');
    menu.className = 'menu col s6 offset-s6';
    menu.id = `${vm.id}propertiesMenu`;
    menu.style.display = 'none';
    currentMenuId = menu.id;
    let container = document.createElement('div');
    container.className = 'propertiesContainer';
    let name = vm.id;
    container.innerHTML = `<h5 class="menuTitle">Name:</h5> <span class="menuTitle" >${name}</span>`;
    menu.appendChild(container);
    // create drop dropdown to add vulnerabilities
    let vlunContainer = document.createElement('div');
    vlunContainer.id = 'vulnContainer';
    vlunContainer.innerHTML = "<h5 class='menuTitle'>Vulnerabilities:<h5>";
    for (let opt of vulnerabilities) {
        let option = document.createElement('div');
        option.id = opt.name;
        let flag = false;
        for (let vuln of vm.vulnerabilities) {
            if (vuln.name === opt.name) {
                flag = true;
            }
        }
        option.className = 'optionSel';
        let box = document.createElement('input');
        box.className ='optionCheckbox';
        box.type = 'checkbox';
        if (flag) {
            box.checked = 'checked';
            option.className = 'optionSel checked';
        }
        else {
            box.checked = '';
            option.className = 'optionSel';
        }
        // check to see if the vulnerabilities are already added and if so make sure the option is checked

        option.onclick = (event) => {
            if (box.checked) {
                box.checked = '';
                option.className = 'optionSel';
            }
            else {
                box.checked = 'checked';
                option.className = 'optionSel checked';
            }
        }
        let span = document.createElement('span');
        span.innerHTML = opt.name;
        option.appendChild(box);
        option.appendChild(span);
        vlunContainer.appendChild(option);
    }
    menu.appendChild(vlunContainer);
    // get the services that the user can run on that vm
    let servContainer = document.createElement('div');
    servContainer.innerHTML = "<h5 class='menuTitle'>Services:<h5>";
    for (let opt of services) {
        let option = document.createElement('div');
        option.className = 'optionSel';
        option.id = opt.name;
        let box = document.createElement('input');
        box.type = 'checkbox';
        box.className ='optionCheckbox';
        let flag = false;
        for (let serv of vm.services) {
            if (serv.name === opt.name) {
                flag = true;
            }
        }
        option.className = 'optionSel';
        box = document.createElement('input');
        box.type = 'checkbox';
        if (flag) {
            box.checked = 'checked';
            option.className = 'optionSel checked';
        }
        else {
            box.checked = '';
            option.className = 'optionSel';
        }
        option.onclick = (event) => {
            if (box.checked) {
                box.checked = "";
                option.className = 'optionSel';
            }
            else {
                box.checked = 'checked';
                option.className = 'optionSel checked';
            }
        }
        let span = document.createElement('span');
        span.className = 'optionSpan';
        span.innerHTML = opt.name;
        option.appendChild(box);
        option.appendChild(span);
        servContainer.appendChild(option);
    }
    menu.appendChild(servContainer);
    let button = document.createElement('button');
    button.innerHTML = 'Ok';
    button.onclick = (event) => {
        for (let machine of conf.machines) {
            if (machine.id === name) {
                for (let option of vlunContainer.children) {
                    if (option.className.includes('optionSel')) {
                        if (option.className.includes('checked')) {
                            let commands = [];
                            for (let opt of vulnerabilities) {
                                if (opt.name === option.id) {
                                    commands = opt.commands;
                                }
                            }
                            if (machine.vulnerabilities) {
                                machine.vulnerabilities.push({
                                    name: option.id,
                                    commands: commands
                                });
                            }
                            else {
                                machine.vulnerabilities = [
                                    {
                                        name: option.id,
                                        commands: commands
                                    }
                                ]
                            }

                        }
                    }
                }
                for (let option of servContainer.children) {
                    if (option.className.includes('optionSel')) {
                        if (option.className.includes('checked')) {
                            let commands = [];
                            for (let opt of services) {
                                if (opt.name === option.id) {
                                    commands = opt.commands;
                                }
                            }
                            if (machine.services) {
                                machine.services.push({
                                    name: option.id,
                                    commands: commands
                                });
                            }
                            else {
                                machine.services = [
                                    {
                                        name: option.id,
                                        commands: commands
                                    }
                                ]
                            }

                        }
                    }
                }
            }
            addData(machine);
        }
        removePropertiesMenu();
    }
    document.getElementById('dragArea').appendChild(menu);
}


// wipe data on boot
clearData();

// document.onmousedown = (event) => handleMouseEvent(event);