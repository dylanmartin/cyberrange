// import required npm libraries
const express = require("express");
const fs = require("fs");
const bodyParser = require("body-parser");
const cookieParser = require('cookie-parser');
const crypto = require('crypto');
const Emailhandler = require("./assets/EmailHandler.js");
const episodeDir = "./episodes";
const app = express();
const port = 8080;
const router = express.Router();
const jwt = require('jsonwebtoken');
var ip = require('ip');


const jwtSecret = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)


// detect the operating system for determining which scripts to run
const os = process.platform;
// console.log("os is " + os)

//list of the running 
let runningEpisodes = []

//used in startEpisodes to determine which containers belong to
//an episode
let prevContainerNum = 0

//the id for a game 
let gameID = 0
process.env.TZ = 'America/New_York';


// set episodes to a list of all the current available episodes
let episodes;
let episodesInfo = [];
fs.readdir(episodeDir, (err, files) => {
    episodes = files;
    for (let i = 0; i < episodes.length; i++) {
        if (episodes[i] === 'Readme.md') {
            episodes.splice(i, 1);
        }

    }

    for (var i = 0; i < episodes.length; i++) {
        let information = JSON.parse(fs.readFileSync(__dirname + `/episodes/${episodes[i]}/config.json`, 'utf8'));
        //console.log(information[episodes[i]]);
        episodesInfo.push(information[episodes[i]]);
    }
    // console.log(episodes)
})

//Creates array of episodes as JSONs from each episodes config.json


// create dictionary of episode json objects
//var episodeJSON = fs.readFileSync("./episodes.json", 'utf8');
//var episodesJSON = JSON.parse(episodeJSONFile);

const Sequelize = require('sequelize')
const sequelize = new Sequelize('database', 'username', 'password', {
    host: 'localhost',
    dialect: 'sqlite',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    storage: './database.sqlite'
})

sequelize.authenticate()
    .then(() => {
        // console.log('Connection has been established successfully.')
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err)
    })

const User = sequelize.define('user', {
    firstName: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null
    },
    lastName: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null
    },
    userName: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    episodeInfo: {
        type: Sequelize.JSON,
        defaultValue: {}
    },
    role: {
        type: Sequelize.STRING,
        allowNull: false
    },
    pwHash: {
        type: Sequelize.STRING,
        defaultValue: null
    }
})

User.sync()
    .then(() => {
        return User.findAll()
    })
    .then(users => {
        // users.destroy({force: true})
        // console.log(`\n\n${users.length} users in db\n\n`)
        return User.findOrCreate({ where: { email: 'admin@udel.edu' }, defaults: { userName: 'admin', role: 'admin', pwHash: crypto.createHash('md5').update('admin').digest('hex') } })
    })
    .then(([user, created]) => {
        if (created) {
            // console.log('created the admin user')
        } else {
            // console.log('admin user already existed')
        }
    })
    .catch((err) => {
        if (err.name === 'SequelizeUniqueConstraintError') {
            // console.log('hey\n\n\n\n\nhey')
            // This is how we handle sequelize errors
        }
        console.error(err)
    })

// configure server
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser());
app.get("/", function (req, res) {
    // implement redirect logic here
    res.redirect('/login');
})
app.route('/login').get(
    (req, res) => {
        res.sendFile(__dirname + "/login.html");
    }
)
app.use("/assets", express.static(__dirname + "/assets"))
app.use('/', express.static(__dirname + "/assets"));

//Casey Rock 07/1/2019
//gets the forgot login page
app.get("/forgotLogin", function (req, res) {
    try {
        res.sendFile(__dirname + "/forgotLogin.html")
    } catch (error) {
        console.log(error)
    }
});


//Casey Rock
//9/23/2019
//
app.route('/home').get(authenticateRoute, function (req, res) {
    try {
        res.setHeader('Content-Type', 'text/html');
        res.sendFile(__dirname + "/home.html")
    } catch (error) {
        console.log(error)
    }
})


app.get("/createGame", authenticateRoute, function (req, res) {
    try {
        res.sendFile(__dirname + "/createGame.html")
    } catch (error) {
        console.log(error)
    }
})

//Casey Rock 07/1/2019
//gets the create login page
app.get("/createNewAccount", function (req, res) {
    try {
        res.sendFile(__dirname + "/createNewAccount.html")
    } catch (error) {
        console.log(error)
    }
})

app.get("/admin", authenticateRoute, function (req, res) {
    try {
        res.sendFile(__dirname + "/admin.html")
    } catch (error) {
        console.log(error)
    }
})

app.route('/auth/login')
    .post((req, res, next) => {
        console.log(JSON.stringify(req.body))
        if (!req.body.email || !req.body.pwHash) {
            res.status(400).json({ message: 'username and password hash required' })
        }
        req.body.pwHash = crypto.createHash('md5').update(req.body.pwHash).digest('hex')
        User.findOne({ where: { email: req.body.email } })
            .then((userInfo) => {
                if (req.body.pwHash === userInfo.pwHash) {
                    let expireMinutes = 60;
                    const token = jwt.sign({
                        exp: Math.floor((Date.now() / 1000) + (60 * expireMinutes)),
                        data: req.body.email
                    }, jwtSecret)
                    res.status(200).json({ message: 'logged in', token })
                } else {
                    res.status(401).json({ message: 'invalid password' })
                }
            })
            .catch((err) => {
                res.status(400).json({ message: 'Invalid credentials' })
                console.error(err)
            })
    })

app.route('/auth/register')
    .post((req, res, next) => {
        // hash pw
        if (!req.body.email || !req.body.pwHash) {
            res.status(400).json({ message: 'username and password hash required' })
        }
        req.body.pwHash = crypto.createHash('md5').update(req.body.pwHash).digest('hex');
        User.findOrCreate({ where: { email: req.body.email, pwHash: req.body.pwHash }, defaults: { userName: req.body.username, role: 'unassigned', firstName: req.body.firstname, lastName: req.body.lastname } })
            .then(([user, created]) => {
                if (created) {
                    res.status(200).json({ message: 'User successfully registered' })
                } else {
                    res.status(400).json({ message: 'User already exists!' })
                }
            })
            .catch((err) => {
                res.status(500).json({ message: 'error' })
                console.error(err)
            })
    })

app.route('/auth/changePassword')
    .post((req, res, next) => {
        // hashpw
        if (!req.body.email || !req.body.oldPwHash) {
            res.status(400).json({ message: 'username and password hash required' })
        }
        req.body.oldPwHash = crypto.createHash('md5').update(req.body.oldPwHash).digest('hex')
        req.body.newPwHash = crypto.createHash('md5').update(req.body.newPwHash).digest('hex')
        User.findOne({ where: { email: req.body.email, pwHash: req.body.oldPwHash } })
            .then((user) => {
                return User.update({ pwHash: req.body.newPwHash }, { where: { userName: req.body.username, email: req.body.email, pwHash: req.body.oldPwHash }, returning: true, plain: true })
            })
            .then((result) => {
                res.status(200).json({ message: 'Changed password' })
            })
            .catch((err) => {
                res.status(500).json({ message: 'Invalid credentials' })
                console.error(err)
            })
    })

//Casey Rock
//1/13/2020
//sends an array of all the running episodes 
app.get("/runningEpisodes", authenticateRoute, function (req, res) {
    try {
        res.json({ runningEp: runningEpisodes })
    } catch (error) {
        console.log(error)
    }
})


// Dylan Martin 06/28/2019
// Send starts a specified episode
// Gets data from header
app.post('/startEpisode', authenticateRoute, async (req, res) => {
    try {
        let episode = req.body.episode


        let rep = await startEpisode(episode, req.body.red, req.body.blue, req.body.creator, req.body.password);

        res.json(rep)
    } catch (error) {
        console.log(error)
    }
})

app.post('/playerComments', authenticateRoute, function (req, res) {
    try {
        for (let i = 0; i < runningEpisodes.length; i++) {

            //find the specific game
            if (runningEpisodes[i].gameID == req.body.id) {

                //find the team the player is on 
                if (req.body.team == 'blue') {
                    //find the player
                    for (let j = 0; j < runningEpisodes[i].blueTeam.length; j++) {
                        if (runningEpisodes[i].blueTeam[j].username == req.body.player) {
                            runningEpisodes[i].blueTeam[j].comments.push(req.body.comment)
                        }
                        break

                    }
                } else {
                    for (let j = 0; j < runningEpisodes[i].redTeam.length; j++) {
                        if (runningEpisodes[i].redTeam[j].username == req.body.player) {
                            runningEpisodes[i].redTeam[j].comments.push(req.body.comment)
                        }
                        break
                    }
                }
            }
            break
        }

    } catch (error) {
        console.log(error)
    }
})


//Diane Vinson 1/14/20
//returns an array of searched episodes
app.post('/searchEpisodes', authenticateRoute, (req, res) => {
    try {
        let searchString = req.body.data;
        let episodelist = searchEpisodeList(searchString);
        res.json({ searchedEpisodes: episodelist })
    } catch (error) {
        console.log(error)
    }
})

//Casey Rock4
//runs the kill contianers function 
app.post('/killContainers', authenticateRoute, (req, res) => {
    try {
        killContainer()
    } catch (error) {
        console.log(error)
    }
})

//Casey Rock 7/12/19
//handles the messgaes for the redteam emails
// app.post("/redEmails", authenticateRoute, (req, res) => {
//     try {
//         res.sendFile(__dirname + `/episodes/${req.body.data}/redTeamBriefing.html`)
//     } catch (error) {
//         console.log(error)
//     }


// })

//Casey Rock 7/12/19
//handles the messgaes for the redteam emails
// app.post("/bluePlayers", authenticateRoute, (req, res) => {
//     try {
//         res.sendFile(__dirname + `/episodes/${req.body.data}/blueTeamBriefing.html`)
//     } catch (error) {
//         console.log(error)
//     }
// })


//Casey Rock 7/12/19
//sends the red and blue team messages
// app.post("/sendEmails", authenticateRoute, (req, res) => {
//     try {
//         // Emailhandler.BlueTeamEmail(req.body.blueData, req.body.blueMessages)
//         // Emailhandler.RedTeamEmail(req.body.redData, req.body.redMessages)
//     } catch (error) {
//         console.log(error)
//     }
// })

// Dylan Martin 06/28/2019
// responds an array of the availabe episodes found in body.episodes
//NO LONGER BEING USED BY admin.html
app.post("/getEpisodes", authenticateRoute, (req, res) => {
    try {
        res.json({ episodes: getEpisodes() })
    } catch (error) {
        console.log(error)
    }
})

//Casey Rock
//1/27/20
//allows the scorebot and the home page to stop a certian episode
app.post("/stopEpisode", authenticateRoute, (req, res) => {
    var { spawnSync, execSync } = require("child_process")
    try {
        let apikey = "712e06c5a95a33b79d7a417bdf33b91f49c3bb4d5d90b4637d8e3e174ab4e940a78cdb9dbac047c6cb5b58baa667ed55172f04f1a8803076ac4649d338e63e93"
        for (let j = 0; j < runningEpisodes.length; j++) {
            if (runningEpisodes[j].gameID == req.body.id && req.body.apikey == apikey) {
                for (let i = 0; i < runningEpisodes[j].containers.length; i++) {

                    execSync(`docker kill ${runningEpisodes[j].containers[i].id}`)
                    execSync(`docker rm ${runningEpisodes[j].containers[i].id}`)

                    prevContainerNum--
                }
                runningEpisodes.length < 1 ? runningEpisodes.pop() : runningEpisodes.splice(j, 1)
            }
        }

    } catch (error) {
        console.log(error)
    }
})

// Dylan Martin 07/1/2019
// Refactored by Diane Vinson 01/08/2020
// returns html briefing
/*
app.post("/brief", authenticateRoute, function (req, res) {
    try {
        var episodeNum = req.body.data.replace('episode', '');
        res.json(episodeJSON[episodeNum - 1]);
        //res.sendFile(__dirname + `/episodes/${req.body.data}/adminBriefing.html`)
    } catch (error) {
        console.log(error)
    }


})*/

app.post("/brief", authenticateRoute, function (req, res) {
    try {
        res.sendFile(__dirname + `/episodes/${req.body.data}/config.json`)
    } catch (error) {
        console.log(error)
    }


})

var myutil = require('./app/server/util')

//Diane Vinson
// 1/21/2020
//Takes episode name and container name and returns username and password
app.post("/SSHCreds", authenticateRoute, function (req, res) {
    try {
        var contents = fs.readFileSync(__dirname + `/episodes/${req.body.episode}/config.json`, 'utf8')
        let imageName = req.body.image;
        let configObject = JSON.parse(contents);
        let loginCreds = configObject.containers[imageName]
        myutil.setCreds(loginCreds.Username, loginCreds.Password)
        res.json({ ip: ip.address() });
    } catch (error) {
        console.log(error);
    }
})


app.use("/", router)
app.listen(port)

//Casey Rock
//1/13/2020
//sends an array of all the running episodes as a JSON object
app.get("/runningEpisodes", authenticateRoute, function (req, res) {
    try {
        res.json({ runningEp: runningEpisodes })
    } catch (error) {
        console.log(error)
    }
})



// Dylan Martin 06/28/2019
// Runs the docker image
// TODO: implement command that runs docker image
const startEpisode = async (episode, redTeam, blueTeam, creatorName, gamePassword) => {

    return new Promise(
        async (res, err) => {

            let newArr = [];
            let gameTime = new Date()
            var { spawnSync, execSync, exec } = require("child_process")
            var command;
            var networkCommand;
            if (os === 'win32' || os === 'win64') {
                // console.log(`sh episodes\\${episode}\\RunEpisodeWindows.sh`);
                // command = spawnSync(`sh episodes\\${episode}\\RunEpisodeWindows.sh`)
                const shell = require('node-powershell');
                const ps = new shell({
                    executionPolicy: 'Bypass',
                    noProfile: true
                });
                ps.addCommand(`sh episodes\\${episode}\\RunEpisodeWindows.sh`);
                let output = await ps.invoke();
                // console.log(output);
            } else {
                if (fs.existsSync(`./episodes/${episode}/ScoreBotFolder`)) {
                    fs.writeFileSync(`./episodes/${episode}/ScoreBotFolder/gameid.txt`, gameID)
                }

                let c2 = exec(`./episodes/${episode}/RunEpisode.sh`, () => {
                    console.log(c2.toString());
                    command = execSync('docker ps');
                    let containerArr = command.toString().split('\n');
                    console.log(command.toString());
                    // parse data and get back image name, container name, and container id
                    let indexes = {
                        containerID: containerArr[0].indexOf('CONTAINER ID'),
                        image: containerArr[0].indexOf("IMAGE"),
                        port: containerArr[0].indexOf("PORTS"),
                        container: containerArr[0].indexOf("NAMES"),
                    }
                    //handles all the infor for the containers 
                    let numOfContainerPerEpisode = 0
                    for (let indexOfContainer = 1; indexOfContainer < (containerArr.length - prevContainerNum); indexOfContainer++) {
                        numOfContainerPerEpisode++
                        let line = containerArr[indexOfContainer];
                        if (line !== '') {
                            // get container id
                            let index = indexes.containerID
                            let dat = ''
                            while (line[index] !== ' ') {
                                dat += line[index];
                                index++;
                            }
                            let id = dat;
                            // console.log(id);
                            // get image name
                            dat = '';
                            index = indexes.image;
                            while (line[index] !== ' ') {
                                dat += line[index];
                                index++;
                            }
                            let imageName = dat;

                            dat = ''
                            index = indexes.port
                            if (line[index + 8] !== ' ') {
                                for (let i = 0; i < 4; i++) {
                                    dat += (line[index + 8])
                                    index++
                                }
                            } else {
                                dat = null
                            }
                            let ports = dat
                            // console.log(imageName)
                            // get container name
                            dat = '';
                            index = indexes.container;
                            while (index < line.length) {
                                dat += line[index];
                                index++;
                            }
                            let containerName = dat;
                            // console.log(containerName

                            let contents = fs.readFileSync(__dirname + `/episodes/${episode}/config.json`, 'utf8')
                            let configObject = JSON.parse(contents);
                            let containers = configObject.containers[imageName]

                            newArr.push({
                                id: id,
                                imageName: imageName,
                                ports: ports,
                                containerName: containerName,
                                hidden: containers.hidden,
                                team: containers.team
                            });

                        }

                    }

                    //handles all the data for the networks 
                    // for (let j = 3; j < networkIndex.length; j++){

                    // }
                    gameID++;
                    prevContainerNum += numOfContainerPerEpisode

                    runningEpisodes.push({
                        gameID: gameID,
                        gameTime: (gameTime.getMonth() + 1) + '/' + (gameTime.getDate()) + "/" + (gameTime.getFullYear()) + " " + (gameTime.getHours()) + ":" + (gameTime.getMinutes()) + ":" + (gameTime.getSeconds()),
                        episodeName: episode,
                        containers: newArr,
                        redTeam: redTeam,
                        blueTeam: blueTeam,
                        creator: creatorName,
                        password: gamePassword,

                    })
                    //runningEpisodes.push(episodes)
                    res(runningEpisodes)
                });




            }



        }
    )

}


//Casey Rock 7/18/2019
//kills all the running contianers
const killContainer = async () => {
    var { spawnSync } = require("child_process")
    var command;
    if (os === 'win32' || os === 'win64') {


        const shell = require('node-powershell');
        const ps = new shell({
            executionPolicy: 'Bypass',
            noProfile: true
        });
        ps.addCommand(`sh assets\\StopContainers.sh`);
        let output = await ps.invoke();

    } else {

        command = spawnSync(`./assets/StopContainers.sh`)
    }
    var data = command.stdout

    return data.toString()
}

//kills the container on statup
killContainer();

// Dylan Martin 06/28/2019
// Returns an array of the episodes defined near line 16
const getEpisodes = () => {
    return episodes
}

//Diane Vinson
function searchEpisodeList(searchString) {
    var retList = [];
    var i;
    for (i = 0; i < episodesInfo.length; i++) {
        if ((episodesInfo[i].Title.toLowerCase()).includes(searchString.toLowerCase()) || episodes[i].includes(searchString)) {
            //let key = "episode" + i;
            retList.push(episodesInfo[i]);
        }
    }
    return retList;
}

function authenticateRoute(req, res, next) {
    if (req.cookies.token) {
        jwt.verify(req.cookies.token, jwtSecret, (err, decoded) => {
            if (err) {
                // console.error(err)
                //res.redirect('/login')

                //res.status(400).json({ message: 'expired, log in again' })
                res.sendFile(__dirname + "/expiredToken.html")

                return;
            } else {
                // console.log(decoded)
                // res.status(200).json({ message: 'good token' })
                next();
            }
        })
    } else {
        // console.log('invalid request');
        res.status(400).json({ message: 'invalid request' })
        return;
    }
}
