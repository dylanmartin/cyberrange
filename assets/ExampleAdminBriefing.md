# Episode Briefing

The blue team has a vulnerable server sending encrypted information to a vulnerable client. However the information is encrypted with the key being stored on the server. The goal of the red team is to break into the server, take the key, and decrypt the network traffic to get the flag. 

