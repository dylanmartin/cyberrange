#Stops all current running containers.

echo "Killing Currently Running Containers..."
docker kill $(docker ps -q)
docker rm $(docker ps -a -q)
docker network prune -f
echo "Finished"