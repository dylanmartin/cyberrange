const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "cyberrangegm@gmail.com",
    pass: "UDcyb3rwarri0r!"
  }
});
RedTeamEmail = (EmailList, Message) => {
  console.log(EmailList);
  EmailList.forEach(Receiver => {
    let mailOptions = {
      from: "cyberrangegm@gmail.com",
      to: Receiver,
      subject: "RedTeam",
      html: `<h1>Red Team</h1><div>${Message}</div>`
    };
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
  });
};
BlueTeamEmail = (EmailList, Message) => {
  EmailList.forEach(Receiver => {
    let mailOptions = {
      from: "cyberrangegm@gmail.com",
      to: Receiver,
      subject: "BlueTeam",
      html: `<h1>Blue Team</h1><div>${Message}</div>`
    };
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
  });
};

let BothTeamEmail = (RedEmails, BlueEmails, GameCode) => {
  RedTeamEmail(RedEmails, GameCode);
  BlueTeamEmail(BlueEmails, GameCode);
};

module.exports.BothTeamEmail = BothTeamEmail;
module.exports.RedTeamEmail = RedTeamEmail;
module.exports.BlueTeamEmail = BlueTeamEmail;
