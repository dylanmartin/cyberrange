

addNote = () => {
    let list = document.getElementById('note-list');
    let li = document.createElement('li');
    let input = document.getElementById('txtSearch');
    li.innerHTML = input.value;
    list.appendChild(li)
    input.value = '';
}
// Dylan Martin 
// 01/30/2020
// Checks to see if enter key is pressed and if so adds the note
searchKeyPress = (e) => {
    e = e || window.event;
    if (e.keyCode == 13)
    {
        addNote(e);
        return false;
    }
    return true;
}