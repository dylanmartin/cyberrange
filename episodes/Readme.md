# Episode Creation Guide

In order to craete an episode for use with the cyber range teechnology you need five required files. Insidee a folder named after the eepisode you need a Red Team Briefing, Blue Team Briefing, Episode Overview, Start Script for Windows, and Start Scripts for Linux. Every episode should look similar to the example file heirarchy shown below

```
EpisodeName
+-- _clientFolder
|   +-- dockerfile
|   +-- someFile.py
+-- _serverFolder
|   +-- dockerfile
|   +-- someFile.js
+-- adminBriefing.html
+-- blueTeamBriefing.html
+-- redTeamBriefing.html
+-- RunEpisode.sh
+-- RunEpisodeWindows.sh
```



## Red Team Briefing

The red team briefing should be saved in a file called "readTeamBriefing.html". This file will be sent via email to the participants the admin specified uppon the start of a new session. This file should contain anything that anyone participating on the red team should know, like the target's IP address, a summary of the mission's objectives, or any prerequesite knowledge that the attackers should have.

## Blue Team Briefing

The blue team briefing should be saved in a file called "blueTeamBriefing.html". The file will be sent to the participants the blue team with the information they need to ssh into the containers they need to defend. This file should contain any information the blue team needs to defend the target like ssh login cridentials, IP address of target, information about any running processes, a list of processes that need to run or ports that need to remain open, and a general summary of the episode and the blue team's objectives. 

## Episode Summary

Create a file called "adminBriefing.html". This file will be shown to who is running the episode during the episode selection process. This file should have a summary of the eipsodee, the difficulty, objectives for both the red and blue teams, and required skills to complete the challange.

## Start Scripts

Start Scripts for both Linux and Windows called RunEpisode.sh and RunEpisodeWindows.sh respectively. They should launch the episode with the same IP and login cridentials that are specified in the red and blue team briefings.
