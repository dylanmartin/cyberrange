# Debrief

As you found, this episode was fairly simple, and tested your ability to sift through a scanner's results. With a vanilla RPi on the network, once you found it's IP, you could log in using it's default credentials. At the end of a scan, most scanners will attempts to brute force some ssh username/password combinations that are most popular. pi:raspberry, being the default for raspbian, is quite common.

## Further steps

1) Never leave a RPi (or any device) with default credentials on the network

2) Never use Password protection with SSH, use keys

3) Constantly scan for new devices coming on to your network, and identify what vulnerabilities they may ahve