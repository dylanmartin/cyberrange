# NASA Jet Propulsion lab Defense

### April 2018

You have recently discovered from one of your employees that there is a RPi on the network. As a security specialist, you know the shortfalls of a default RPi configuration. Your objective is to find it and secure it to protect what files may live on the device.