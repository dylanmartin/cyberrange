# NASA Jet Propulsion lab Defense

### April 2018

You have recently discovered that there is a RPi on the network. As you well know from your experience in both remote exploitation and working with RPi's yourself, they have some initial security shortcomings if they are not configured properly. Chances are it isn't. Can you break into the device and steal secret NASA files?