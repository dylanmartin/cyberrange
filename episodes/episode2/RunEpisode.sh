#Script to run Dockerfile and run Containers for Episode2

#Don't know if any other files have to be run with the Dockerfile, 
#Ask Dan

DOCKERFILE_PATH="."

echo "Begin Episode 2"
docker login -u cyberrangeudel -p SuperSecureLogon00
docker pull ubuntu:18.04
docker build -t episode2 $DOCKERFILE_PATH
port="3000"
httsPorts="7000"
subnet="10"
while [ true ]
do
    cmd=`lsof -i:$port`
    if [ -z "$cmd" ]
    then
        break
    fi
    port=$(( $port + 1 ))

    server=$port
    subnet=$(( $subnet + 1))
done
echo $port
docker network create --driver=bridge --subnet=$subnet.34.5.0/24 server2$port #<name-of-server>

docker run --network="server2$port" --ip $subnet.34.5.2 -itd -p $port:22  -p $httsPorts:8080 --name=episode2_container$port episode2
docker exec episode2_container$port service ssh start

#docker network connect --ip 178.34.5.2 server2 episode2_container
#echo ''
echo "{\"blueTeamPort\":\"$[$port-1]\", \"redTeamPort\":\"$port\"}"