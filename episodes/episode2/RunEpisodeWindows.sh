#Script to run Dockerfile and run Containers for Episode2 on WINDOWS

#Don't know if any other files have to be run with the Dockerfile, 
#Ask Dan

DOCKERFILE_PATH="\Users\pentest\CyberRange\episodes\episode2"

echo "Begin Episode 2"
docker login -u cyberrangeudel -p SuperSecureLogon00
docker pull ubuntu:18.04
docker build -t episode2 $DOCKERFILE_PATH
docker network create --driver=bridge --subnet=178.34.5.0/24 --gateway 178.34.5.1 server2 #<name-of-server>
docker run --network="server2" -itd --name=episode2_container episode2
docker network connect --ip 178.34.5.2 server2 episode2_container