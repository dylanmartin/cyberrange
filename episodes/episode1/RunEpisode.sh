#Script to run Dockerfile and run Containers for Episode1

#Don't know if any other files have to be run with the Dockerfile, 
#Ask Dan

#DOCKERFILE_CLIENT_PATH="./episodes/episode1"
DOCKERFILE_CLIENT_PATH="./episodes/episode1/ClientFolder"
DOCKERFILE_ATTACK_PATH="./episodes/episode1/AttackerFolder"

echo "Begin Episode 1"
docker login -u cyberrangeudel -p SuperSecureLogon00
docker pull ubuntu:18.04
docker build -t client_image $DOCKERFILE_CLIENT_PATH
docker build -t attacker_image $DOCKERFILE_ATTACK_PATH


sshPort="5000"
httpPort="7000"
subnet="10"
while [ true ]
do
    cmd=`sudo lsof -i:$sshPort`
    if [ -z "$cmd" ]
    then
        break
    fi
    sshPort=$(( $sshPort + 1 ))

    server=$sshPort
    subnet=$(( $subnet + 1))
done
echo $sshPort

while [ true ]
do
    cmd=`sudo lsof -i:$httpPort`
    if [ -z "$cmd" ]
    then
        break
    fi
    httpPort=$(( $httpPort + 1 ))

done
echo $httpPort

docker network create --driver=bridge --subnet=$subnet.67.89.0/16 server1$server
docker run --network="server1$server" --ip $subnet.67.89.2 -itd -p $sshPort:22 --name=client_container$sshPort client_image
docker exec client_container$sshPort service ssh start


while [ true ]
do
    cmd=`sudo lsof -i:$sshPort`
    if [ -z "$cmd" ]
    then
        break
    fi
    sshPort=$(( $sshPort + 1 ))

done
echo $sshPort


docker run --network="server1$server" --ip $subnet.67.89.3 -itd -p $sshPort:22 --name=attacker_container$sshPort attacker_image
docker exec attacker_container$sshPort service ssh start

echo "{\"blueTeamPort\":\"$[$sshPort-1]\", \"redTeamPort\":\"$sshPort\"}"
