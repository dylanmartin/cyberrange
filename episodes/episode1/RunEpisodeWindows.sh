#Script to run Dockerfile and run Containers for Episode1 on WINDOWS

#Don't know if any other files have to be run with the Dockerfile, 
#Ask Dan

DOCKERFILE_PATH="\Users\pentest\CyberRange\episodes\episode1"

echo "Begin Episode 1"
docker login -u cyberrangeudel -p SuperSecureLogon00
docker pull ubuntu:18.04
docker build -t episode1 $DOCKERFILE_PATH
docker network create --driver=bridge --subnet=125.67.89.0/24 --gateway 125.67.89.1 server1 #<name-of-server>
docker run --network="server1" -itd --name=episode1_container episode1
docker network connect --ip 125.67.89.2 server1 episode1_container