#RUN THIS FILE TO START EPISODE3 ON WINDOWS
#IN ORDER FOR THIS TO BE RUN WITHIN POWERSHELL, YOU MUST:
#  - INSTALL GIT
#  - EDIT ENVIORTMENT VARIABLE AND ADD THE NEW FOLLOWING PATHS:
#       - C:\Program Files\Git\bin
#       - C:\Program Files\Git\cmd

#TO RUN SCRIPT ON TERMINAL: sh RunEpisode3Windows.sh

#Path to Dockerfiles, different on each computer.
#Update Path for use
#DOCKERFILE_SERVER_PATH="\Users\pentest\CyberRange\episodes\episode3\ServerFolder"
#DOCKERFILE_CLIENT_PATH="\Users\pentest\CyberRange\episodes\episode3\ClientFolder"
#Script which creates client and server images
#CONTAINER_SCRIPT="\Users\pentest\Desktop\CyberRange\episodes\episode3\CreateContainers.sh"
DOCKERFILE_SERVER_PATH".\ServerFolder"
DOCKERFILE_CLIENT_PATH=".\ClientFolder"
#Script which creates client and server images
CONTAINER_SCRIPT=".\CreateContainers.sh"
echo "Begin Episode 3"
docker login -u cyberrangeudel  -p SuperSecureLogon00
docker pull ubuntu:18.04
docker build -t server_image $DOCKERFILE_SERVER_PATH
docker build -t client_image $DOCKERFILE_CLIENT_PATH
# docker build -t
$CONTAINER_SCRIPT
docker network inspect server0 #Inspecting the network to see if script was sucessful

#server_image (server) image runs as server_user
#client_image (client) image runs as root
