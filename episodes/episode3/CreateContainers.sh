port="5000"
subnet="10"
while [ true ]
do
    cmd=`lsof -i:$port`
    if [ -z "$cmd" ]
    then
        break
    fi
    port=$(( $port + 1 ))

    server=$port
    subnet=$(( $subnet + 1))
done
echo $port
docker network create --driver=bridge --subnet=$subnet.18.12.0/16 server3$server #<name-of-server>
docker run --network="server3$server" --ip $subnet.18.12.2 -d -p $port:22 --name=server_container$port server_image #<image-name:latest>
docker exec server_container$port service ssh start
docker exec server_container$port /etc/init.d/xinetd start

while [ true ]
do
    cmd=`lsof -i:$port`
    if [ -z "$cmd" ]
    then
        break
    fi
    port=$(( $port + 1 ))
done
echo $port
# docker exec server_container$port /etc/init.d/xinetd start
docker run --network="server3$server" --ip $subnet.18.12.3 -d -p $port:22 --name=client_container$port client_image #<iamge-name:latest>
docker exec client_container$port service ssh start
docker exec client_container$port /etc/init.d/xinetd start
while [ true ]
do
    cmd=`lsof -i:$port`
    if [ -z "$cmd" ]
    then
        break
    fi
    port=$(( $port + 1 ))
done
echo $port
docker run --network="server3$server" --ip $subnet.18.12.4 -d -p $port:22 --name=attacker_container$port attacker_image
docker exec attacker_container$port service ssh start
# echo "{\"blueTeamPort\":\"$server\", \"redTeamPort\":\"$port\"}"
while [ true ]
do
    cmd=`lsof -i:$port`
    if [ -z "$cmd" ]
    then
        break
    fi
    port=$(( $port + 1 ))
done
docker run --network="server3$server" --ip $subnet.18.12.5 -d -p $port:22 --name=score_container$port scorebot_image
docker exec score_container$port service ssh start
echo "{\"blueTeamPort\":\"$server\", \"redTeamPort\":\"$port\"}"
