# Red Team War Games
## Episode 2: Inventorious

### Difficulty: 5/10

## IMPORTANT

**DO NOT SCAN DEVICES YOU ARE NOT AUTHORIZED TO**
OpenVAS and Nessus both try to access devices in various ways during the later stages of their scans.

OpenVAS will attempt a small ssh bruteforce attack on the server. If successful, you will have committed a felony under the CFAA*. Additionally, it will try to brute force FTP servers.

Nessus will attempt to screenshot VNC servers, and bruteforce FTP servers. These fall under the same as above.

**TL;DR: NEVER SCAN A DEVICE YOU ARE NOT AUTHORIZED TO**

`nmap` by default is fine though...

\*Computer Fraud and Abuse Act (CFAA)
https://en.wikipedia.org/wiki/Computer_Fraud_and_Abuse_Act
Title 18 U.S.Code Chapter 47  Part? § 1030

### Topics:
`linux` `sockets` `web traffic` `secure programing` `encoding`

### Scenario:

You are in charge of updating a web socket service that is running on your clients server. The net traffic generated from this service must be secured. The service down time must be at a minimum. Find the vulnerability in the service's code, patch it an restart the service so it is secure. 

#### Development Hints:

You are going to want to understand how data is sent over tcp and figure out a way to make data secure while it is in transit. The development is done in python so make sure you understand hashes, decoding, encoding and ecnryption in python.

## Requirements

#### Point System:
Points will be based on how long the service is running and how quickly the red team is able to capture your flag.

#### Reporting:

You will report all attacks on [this attack report google form](). Anytime you make an intrusion, lateral movement, process migration, privilege escallation, persistence, website defacement, file system deletion, etc., you should fill out the form. It is how your Red Team points will be calculated.

#### Attack:
Intercept and decipher the flag from the web traffic on your device.
#### Defense:

Keep your service alive for as long as possible as well as the internal Scoring API.

The service must be able to communicate with the client receiver and the receiver must be able to decode, decipher and or decrypt your message 

## Post Competition

The winner will be determined by who ever has the most amount of points in the end. All source code will be shared by the competitors. In the event that cheating has occurred (source code does not match protection capabilities of the API), the winner will be the highest point holder with legal source code.
