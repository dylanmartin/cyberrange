#!/usr/bin/env python3
#
import socket
import sys
import json
import base64
import string 
from itertools import cycle 
import binascii
host = sys.argv[1]        # Symbolic name meaning all available interfaces is '' should be hard coded <--- USERS IGNORE THIS IS TO BUILD THE DOCKER CONTAINER, WHEN RESTARTING THE SERVICE USE THE IP PROVIDED
port = int(sys.argv[2])    # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))
alphabet = string.printable
SecretKey = 'L33th@x04'
#Client formate Ceasar encryption? -> base64 decoding
#Send Format object{message:message, shift:ceasar shit}
#Do not modify the message you will lose points
FLAG = 'FLAG(the service sucks)'
def secureMessage(message):
    #I think im safe?
    key = SecretKey
    cyphered = ''.join(chr(ord(c)^ord(k)) for c,k in zip(message, cycle(key)))
    return cyphered
# def caesar(plaintext, shift):
#     shifted_alphabet = alphabet[shift:] + alphabet[:shift]
#     table = str.maketrans(alphabet, shifted_alphabet)
#     return plaintext.translate(table)
    
while True:
    try: 
        s.listen(1)
        conn, addr = s.accept()
        print('Connected by', addr)
        try:
            data = conn.recv(1024)
            if not data:
                print('Not Data' + data.decode())
                # print(data)
                continue
            print ("Client Says: "+data.decode())
            #SECURE MESSAGE#
            message = secureMessage(FLAG)
            SendData = message
            print(SendData)
            conn.sendall(SendData.encode())
            if(data.decode() == 'Exit'):
                continue
        except socket.error:
            print ("Error Occured.")
            continue
    except(KeyboardInterrupt):
        s.close()
        break
