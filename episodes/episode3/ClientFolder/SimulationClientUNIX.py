#!/usr/bin/env python
import socket
import time
import json
import binascii
import sys
from itertools import cycle
import base64
sensitiveData = {
    'user':'ConnectionISALIVE',
    'password':'IShouldReallyChangeMypassword1234'
}
connected = False
counter = 0
host = sys.argv[1]
port = int(sys.argv[2])                   # The same port as used by the server
SecretKey = 'L33th@x04'
def secureMessage(message):
    #I think im safe?
    key = SecretKey
    cyphered = ''.join(chr(ord(c)^ord(k)) for c,k in zip(message, cycle(key)))
    return cyphered
while counter < 100:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        connected = True
    except socket.error as error:
        counter += 1
        connected = False
        s.close()
    if(connected == True):
        counter = 0
        try:
            data=(json.dumps(sensitiveData))
            #print(data)
            s.sendall(data.encode('utf-8'))
            data = s.recv(1024)
            message = data.decode()
            m = secureMessage(message)
           # print('Received', repr(data))
            connected = False
            time.sleep(2)
        except(KeyboardInterrupt):
            data = 'Exit'
            s.sendall(data.encode('utf-8'))
            s.close()
    
    time.sleep(2)