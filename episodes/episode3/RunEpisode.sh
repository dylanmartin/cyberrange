#RUN THIS FILE TO START EPISODE3 ON MAC

#Path to Dockerfiles, different on each computer.
#Update Path for use
DOCKERFILE_SERVER_PATH="./episodes/episode3/ServerFolder"
DOCKERFILE_CLIENT_PATH="./episodes/episode3/ClientFolder"
DOCKERFILE_ATTACK_PATH="./episodes/episode3/AttackerFolder"
DOCKERFILE_SCOREBOT_PATH="./episodes/episode3/ScoreBotFolder"
#Script which creates client and server images
CONTAINER_SCRIPT="./episodes/episode3/CreateContainers.sh"

echo "Begin Episode 3"
docker login -u cyberrangeudel -p SuperSecureLogon00
# docker pull ubuntu:18.04
# docker pull kalilinux/kali-linux-docker

# docker build -t server_image ./ServerFolder
# docker build -t client_image ./ClientFolder
#docker login -u cyberrangeudel -p SuperSecureLogon00s
docker pull ubuntu:18.04
docker pull kalilinux/kali-linux-docker
docker build -t server_image $DOCKERFILE_SERVER_PATH
docker build -t client_image $DOCKERFILE_CLIENT_PATH
docker build -t scorebot_image $DOCKERFILE_SCOREBOT_PATH
docker build -t attacker_image $DOCKERFILE_ATTACK_PATH
$CONTAINER_SCRIPT
# docker network inspect server0 #Inspecting the network to see if script was sucessful
