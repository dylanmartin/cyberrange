#!/usr/bin/env python3
#
import os
import socket
import sys
import json
import base64
import math
import string 
import time
from datetime import datetime
from itertools import cycle 
import binascii
import requests

API_ENDPOINT = "188.166.151.43/stopEpisode"
API_KEY = "712e06c5a95a33b79d7a417bdf33b91f49c3bb4d5d90b4637d8e3e174ab4e940a78cdb9dbac047c6cb5b58baa667ed55172f04f1a8803076ac4649d338e63e93"


with open('config.json') as f:
  data = json.load(f)


# # game_id = File_object = open("gameid.txt").read().replace('\n','')

# gameAPIData = {
#     'id':game_id, 
#     'apikey':API_KEY, 
# } 


# Output: {'name': 'Bob', 'languages': ['English', 'Fench']}
print(data)
print("starting")
FLAG = data['flag']
TimeLimit = data['gameTime']
host = sys.argv[1]    # Symbolic name meaning all available interfaces is '' should be hard coded <--- USERS IGNORE THIS IS TO BUILD THE DOCKER CONTAINER, WHEN RESTARTING THE SERVICE USE THE IP PROVIDED
port = int(sys.argv[2])    # Arbitrary non-privileged port
print(host)
print(port)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))
# alphabet = string.printable
# SecretKey = 'L33th@x04'
#Client formate Ceasar encryption? -> base64 decoding
#Send Format object{message:message, shift:ceasar shit}
#Do not modify the message you will lose points
# FLAG = File_object = open("flag.txt").read().replace('\n','')
# print(FLAG)
HasFlag = True
gameOver = False
gameTime = False
totalSeconds = 0
finalTime = 0 
try: 
    a = datetime.now()
    while True:
            b = datetime.now()
            print(a)
            print(b)
            timeDelta = abs(b-a) 
            secondsElapsed = timeDelta.total_seconds()
            totalSeconds += secondsElapsed
            totalSeconds = math.floor(totalSeconds)
            b = a
            a = datetime.now()
            if(totalSeconds >= TimeLimit):
                gameTime = True
            s.listen(1)
            conn, addr = s.accept()
            print('Connected by', addr)
            try:
                data = conn.recv(1024)
                if not data:
                    print('Not Data' + data.decode())
                    # print(data)x
                    continue

                print ("Client Says: "+data.decode())
                inMessage = data.decode()
                # message = secureMessage(FLAG)
                if(inMessage.find("BlueTeam") != -1):
                    # FLAG = inMessage[inMessage.find(':'):]
                    # SendData = "BTReceived"
                    # HasFlag = True
                    # print("Flag Received")
                    print(gameOver)
                    if(gameOver == True and gameTime == False):
                        SendData = "FlagReceived"
                        # sending post request and saving response as response object 
                        r = requests.post(url = API_ENDPOINT, data = data) 
                        
                        # extracting response text  
                        pastebin_url = r.text 
                        # conn.sendall(SendData.encode())
                        if(gameTime == True):
                            SendData = "timeExpired"
                    else:
                        SendData = "BTReceived"
                        
                    conn.sendall(SendData.encode())
                if(inMessage.find("RedTeam") != -1):
                    checkFlag = inMessage[inMessage.find(':')+1:]
                    print(checkFlag == FLAG)
                if(gameTime == True):
                    # SendData = "FlagReceived"
                    conn.sendall(SendData.encode())
                    print("Game over Blue Team Wins! : " + str(totalSeconds))
                if (checkFlag == FLAG and gameTime == False):
                    gameOver = True
                    print("Flag Received")
                    SendData = "FlagReceived"
                    conn.sendall(SendData.encode())
                    print("Game over: " + str(totalSeconds))
                    # r = requests.post(url = API_ENDPOINT, data = gameAPIData)  
                if(inMessage == 'Exit'):
                    continue
            except socket.error:
                print ("Error Occured.")
                continue
except(KeyboardInterrupt):
        # if conn:  # <---s
    s.close()