port="6000"
subnet="100"
while [ true ]
do
    cmd=`lsof -i:$port`
    if [ -z "$cmd" ]
    then
        break
    fi
    port=$(( $port + 1 ))

    server=$port
    subnet=$(( $subnet + 1))
done
echo $port
docker network create --driver=bridge --subnet=$subnet.18.12.0/16 server5$server #<name-of-server>
docker run --network="server5$server" --ip $subnet.18.12.4 -itd -p $port:22 --name=attacker_container$port attacker_image
docker exec attacker_container$port service ssh start


while [ true ]
do
    cmd=`lsof -i:$port`
    if [ -z "$cmd" ]
    then
        break
    fi
    port=$(( $port + 1 ))
done
echo $port
docker run --network="server5$server" --ip $subnet.18.12.5 -itd -p $port:22 --name=metasploitable_container$port metasploitable_image sh -c "/bin/services.sh && bash"


