#RUN THIS FILE TO START EPISODE3 ON MAC

#Path to Dockerfiles, different on each computer.
#Update Path for use
DOCKERFILE_METASPLOITABLE_PATH="./episodes/episode5/MetasploitableFolder"
DOCKERFILE_ATTACK_PATH="./episodes/episode5/AttackerFolder"

#Script which creates client and server images
CONTAINER_SCRIPT="./episodes/episode5/CreateContainers.sh"

echo "Begin Episode 5"
docker login -u cyberrangeudel -p SuperSecureLogon00
# docker pull ubuntu:18.04
# docker pull kalilinux/kali-linux-docker

# docker build -t server_image ./ServerFolder
# docker build -t client_image ./ClientFolder
#docker login -u cyberrangeudel -p SuperSecureLogon00s
docker pull tleemcjr/metasploitable2
docker pull kalilinux/kali-linux-docker
docker build -t metasploitable_image $DOCKERFILE_METASPLOITABLE_PATH
docker build -t attacker_image $DOCKERFILE_ATTACK_PATH
$CONTAINER_SCRIPT
# docker network inspect server0 #Inspecting the network to see if script was sucessful
