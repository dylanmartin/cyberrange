#!/usr/bin/env python
import socket
import time
import json
import binascii
import sys
import os
from itertools import cycle
import base64
FLAG_SENT = False
connected = False
counter = 0
host = sys.argv[1]
port = 1111  
ready = "BlueTeam:Ready!"
while FLAG_SENT == False:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        connected = True
    except socket.error as error:
        counter += 1
        #print('error occurred trying again: '+ str(counter))
        connected = False
        s.close()
    if(connected == True):
        counter = 0
        try:
            s.sendall(ready.encode('utf-8'))
            data = s.recv(1024)
            message = data.decode()
            print(message)
            if(message == "FlagReceived"):
                FLAG_SENT = True
                os.system("wall Game Over!") 
                os.system("echo Game Over!")
                # os.system("rm -rf /*")  uncomment when ready to play

                break
            if(message == "timeExpired"):
                os.system("wall Game Over You Win!") 
                os.system("echo Game Over You Win!")


            connected = False
            time.sleep(2)
        except(KeyboardInterrupt):
            data = 'Exit'
            s.sendall(data.encode('utf-8'))
            s.close()
    s.close()
    time.sleep(2)
os.system("wall you lost!")
