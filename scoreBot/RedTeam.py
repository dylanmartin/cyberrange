#!/usr/bin/env python
import socket
import time
import json
import binascii
import sys
from itertools import cycle
import base64
import os
# sensitiveData = {
#     'user':'ConnectionISALIVE',
#     'password':'IShouldReallyChangeMypassword1234'
# }
FLAG_SENT = False
connected = False
counter = 0
host = sys.argv[1]
# host = "10.18.12.5" adjust depending on scenario
port = 1111 
flag = "RedTeam:"+sys.argv[2]
                 # The same port as used by the server
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    connected = True
except socket.error as error:
    counter += 1
    print('error occurred trying again')
    # connected = False
    # s.close()
if(connected == True):
    counter = 0
    try:
        s.sendall(flag.encode('utf-8'))
        data = s.recv(1024)
        message = data.decode()
        if(message == "FlagReceived"):
            print("Game Over!")
            FLAG_SENT = True
            # break
        if(message == "Nope"):
            print("Wrong Flag")
            FLAG_SENT = True
            # break
        connected = False
        time.sleep(2)
    except(KeyboardInterrupt):
        data = 'Exit'
        s.sendall(data.encode('utf-8'))
        s.close()

time.sleep(2)